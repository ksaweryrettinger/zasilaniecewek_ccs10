#ifndef INCLUDE_PARAMS_H_
#define INCLUDE_PARAMS_H_

/* ----------------------- MODBUS Parameters ------------------------------------*/

#define MB_MODE              (MB_RTU)
#define MB_SLAVEID           (0x01)
#define MB_PORT              (0)
#define MB_BAUDRATE          (921600)
#define MB_PARITY            (MB_PAR_EVEN)

/* ----------------------- I2C Configuration ------------------------------------*/

#define ADDRESS_PCF8574      (0b0100111)
#define I2C_SPEED            (100000)
#define I2C_MASTER_TIMEOUT   (15000)    //equivalent to 30 I2C clock cycles

/* ----------------------- Modbus Register Numbering ----------------------------*/

#define HOLDING_CURRENT_START     (1)
#define HOLDING_CURRENT_END       (16)
#define HOLDING_UART_ERRORS       (17)
#define HOLDING_ECHO_ERRORS       (18)
#define HOLDING_CRLF_ERRORS       (19)
#define HOLDING_RESET_UART        (20)
#define HOLDING_RESET_PSU         (21)

/* ----------------------- INS8250 UART addressing ------------------------------*/

#define UART_START              (0)
#define UART_END                (127)
#define UART_REGNUM             (8) //number of registers per UART module

/* ----------------------- External Register Addressing -------------------------*/

#define REG_8BIT_START      (0)
#define REG_8BIT_END        (1023)
#define REG_16BIT_START     (1024)
#define REG_16BIT_END       (2047)
#define REG_I2C             (2048)

/* ----------------------- Other Constants --------------------------------------*/

#define SYS_DELAY_WRITE      (3) //equivalent to 180ns at 50MHz clock speed
#define SYS_DELAY_READ       (2) //equivalent to 120ns at 50MHz clock speed
#define TIMEOUT_UART_ECHO    (100000) //equivalent to 2ms at 50MHz clock speed
#define TIMEOUT_UART_READ    (112500) //equivalent to 2.25ms at 50MHz clock speed
#define TIMEOUT_UART_WRITE   (150000) //equivalent to 3ms at 50MHz clock speed
#define NUM_POWER_UNITS      (16)
#define MAX_READ_CHARS       (7)
#define CURRENT_CHARNUM      (5)

#endif /* INCLUDE_PARAMS_H_ */
