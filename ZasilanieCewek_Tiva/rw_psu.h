#pragma once

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include <rw_tables.h>
#include <rw_uart.h>
#include <string.h>
#include <stdlib.h>
#include "tm4c123gh6pm.h"
#include "params.h"
#include "mbreg.h"

/* ----------------------- Function prototypes ------------------------------------*/

void xSetupSinglePSU(const CHAR*, UCHAR unitID);  //send setup message to single PSU
void xCheckEcho(const CHAR*);                     //check echo response from all PSUs
void xReadCurrent(void);                          //read current from all PSUs
void xWriteCurrent(USHORT, UCHAR);                //write current (BCD format) to single PSU

/* ----------------------- Global variables ---------------------------------------*/

BOOL bUnitInitialized[NUM_POWER_UNITS];
USHORT usCurrentSettingsBCD[NUM_POWER_UNITS];
