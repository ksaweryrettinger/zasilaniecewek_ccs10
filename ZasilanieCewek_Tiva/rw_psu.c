#include <rw_psu.h>

/*------------------------------------ PSU Communication ---------------------------------------------*/

void xSetupSinglePSU(const CHAR* ASCIIMsg, UCHAR unitID)
{
    //Initialize variables
    CHAR ucEcho[sizeof(ASCIIMsg)] = { 0 };
    UCHAR LSR;
    UCHAR CharNum = 0;
    UCHAR CharNumEcho = 0;
    UCHAR j;

    //Clear UART and ECHO errors
    usMBHoldingReg[HOLDING_UART_ERRORS - 1] &= ~(1 << (unitID - 1));
    usMBHoldingReg[HOLDING_ECHO_ERRORS - 1] &= ~(1 << (unitID - 1));

    while (CharNum != sizeof(ASCIIMsg))
    {
        //Check for incoming data
        LSR = ucUARTRW(0, (unitID - 1)*UART_REGNUM + 5, FALSE);

        if (LSR & 1)
        {
            //Read incoming data
            ucEcho[CharNumEcho] = ucUARTRW(0, (unitID - 1)*UART_REGNUM, FALSE);
            if (CharNumEcho < (sizeof(ASCIIMsg) - 1)) CharNumEcho++;
        }

        //Store any UART errors
        if ((LSR & 0x0E) != 0) usMBHoldingReg[HOLDING_UART_ERRORS - 1] |= (1 << (unitID - 1));

        //Write next character
        ucUARTRW(ASCIIMsg[CharNum], (unitID - 1)*UART_REGNUM, TRUE);
        CharNum++;

        if (CharNum != sizeof(ASCIIMsg))
        {
            //Wait 3ms
            xUARTTimerReset(TIMEOUT_UART_WRITE);
            while (!bUARTTimeout) {}
        }
    }

    //Get 3x ECHO characters (ECHO delay)
    for (j = 0; j < 3; j++)
    {
        //Wait 2ms
        xUARTTimerReset(TIMEOUT_UART_ECHO);
        while (!bUARTTimeout) {}

        //Check for incoming data
        LSR = ucUARTRW(0, (unitID - 1)*UART_REGNUM + 5, FALSE);

        if (LSR & 1)
        {
            ucEcho[CharNumEcho] = ucUARTRW(0, (unitID - 1)*UART_REGNUM, FALSE);
            if (CharNumEcho < (sizeof(ASCIIMsg) - 1)) CharNumEcho++;
        }

        //Store any UART errors
        if ((LSR & 0x0E) != 0) usMBHoldingReg[HOLDING_UART_ERRORS - 1] |= (1 << (unitID - 1));
    }

    //Check for ECHO errors
    if (ucEcho[sizeof(ASCIIMsg) - 1] != 0x0A) ucEcho[sizeof(ASCIIMsg) - 1] = 0x0A; //ensure echo is null terminated
    if (strcmp(ucEcho, ASCIIMsg) != 0) usMBHoldingReg[HOLDING_ECHO_ERRORS - 1] |= (1 << (unitID - 1)); //store echo errors
}

void xReadCurrent(void)
{
    //Initialize variables
    CHAR ASCIIMsg[4] = {'M', 'C', 0x0D, 0x0A};
    CHAR ASCIIReply[NUM_POWER_UNITS][MAX_READ_CHARS] = { 0 };
    CHAR LSR[NUM_POWER_UNITS] = { 0 };
    BOOL bFirstChar = TRUE;
    BOOL bWrite = TRUE;
    UCHAR CharNum = 0;
    UCHAR NumActiveUnits = 0;
    UCHAR SumDR;
    UCHAR i;

    //Clear UART errors
    usMBHoldingReg[HOLDING_UART_ERRORS - 1] = 0;

    //Store number of initialized units
    for (i = 0; i < NUM_POWER_UNITS; i++)
    {
        if (bUnitInitialized[i]) NumActiveUnits++;
    }

    //Read current from initialized units
    while (!(CharNum == MAX_READ_CHARS && !bWrite))
    {
        if (bWrite) //WRITE TO POWER UNITS
        {
            //Check Rx registers
            for (i = 0; i < NUM_POWER_UNITS; i++)
            {
                if (bUnitInitialized[i])
                {
                    LSR[i] = ucUARTRW(0, i * UART_REGNUM + 5, FALSE);

                    //Check if echo is being received
                    if (LSR[i] & 1)
                    {
                        //Read data to avoid overrun errors
                        (void) ucUARTRW(0, i*UART_REGNUM, FALSE);
                        if (bUnitInitialized[i]) bUnitInitialized[i] = FALSE;
                    }

                    //Store any UART errors
                    if ((LSR[i] & 0x0E) != 0) usMBHoldingReg[HOLDING_UART_ERRORS - 1] |= (1 << i);
                }
            }

            //Transfer next character to all units
            for (i = 0; i < NUM_POWER_UNITS; i++)
            {
                if (bUnitInitialized[i]) ucUARTRW(ASCIIMsg[CharNum], i*UART_REGNUM, TRUE);
            }

            CharNum++;

            if (CharNum == sizeof(ASCIIMsg))
            {
                //Switch to read mode
                CharNum = 0;
                bWrite = FALSE;
            }
            else
            {
                //Reset timer and wait 3ms
                xUARTTimerReset(TIMEOUT_UART_WRITE);
                while (!bUARTTimeout) {}
            }
        }
        else //READ RESPONSE FROM POWER UNITS
        {
            if (bFirstChar)
            {
                //Reset timer
                xUARTTimerReset(TIMEOUT_UART_READ);
                bFirstChar = FALSE;
            }

            SumDR = 0;

            //Read Line Status Registers
            for (i = 0; i < NUM_POWER_UNITS; i++)
            {
                if (bUnitInitialized[i])
                {
                    LSR[i] = ucUARTRW(0, i * UART_REGNUM + 5, FALSE);
                    SumDR += LSR[i] & 1;

                    //Store UART errors
                    if ((LSR[i] & 0x0E) != 0) usMBHoldingReg[HOLDING_UART_ERRORS - 1] |= (1 << i);
                }
            }

            //Check if new data ready of timeout
            if (SumDR == NumActiveUnits || bUARTTimeout)
            {
                //Read all new ASCII characters
                for (i = 0; i < NUM_POWER_UNITS; i++)
                {
                    ASCIIReply[i][CharNum] = ucUARTRW(0, i*UART_REGNUM, FALSE);

                    //Reset timer
                    xUARTTimerReset(TIMEOUT_UART_READ);
                }

                CharNum++;
            }
        }
    }

    //Clear <CR><LF> errors
    usMBHoldingReg[HOLDING_CRLF_ERRORS - 1] = 0;

    //Check for errors and store current data in BCD format
    for (i = 0; i < NUM_POWER_UNITS; i++)
    {
        if (bUnitInitialized[i])
        {
            usMBHoldingReg[i] = 0;

            //Detect missing <CR><LF> and store errors
            if (ASCIIReply[i][5] != 0x0D || ASCIIReply[i][6] != 0x0A)
            {
                usMBHoldingReg[HOLDING_CRLF_ERRORS - 1] |= 1 << i;
            }
            else
            {
                //Store current in BCD format
                usMBHoldingReg[i] |= (ASCIIReply[i][0] - '0') << 12;
                usMBHoldingReg[i] |= (ASCIIReply[i][1] - '0') << 8;
                usMBHoldingReg[i] |= (ASCIIReply[i][2] - '0') << 4;
                usMBHoldingReg[i] |= (ASCIIReply[i][4] - '0');
            }
        }

    }
}

void xWriteCurrent(USHORT usCurrentBCD, UCHAR unitID)
{
    //Initialize variables and buffers
    CHAR ASCIIMsg[] = {'P', 'C', 0, 0, 0, '.', 0, 0x0D, 0x0A};
    UCHAR UARTRegAddress;
    UCHAR CharNum = 0;

    //Convert BCD to chars
    ASCIIMsg[2] = ((usCurrentBCD >> 12) & 0xF) + '0';
    ASCIIMsg[3] = ((usCurrentBCD >> 8) & 0xF) + '0';
    ASCIIMsg[4] = ((usCurrentBCD >> 4) & 0xF) + '0';
    ASCIIMsg[6] = (usCurrentBCD & 0xF) + '0';

    //Transfer message
    while (CharNum != sizeof(ASCIIMsg))
    {
       UARTRegAddress = (unitID - 1) * UART_REGNUM;
       ucUARTRW(ASCIIMsg[CharNum], UARTRegAddress, TRUE);
       CharNum++;

       //Wait 3ms
       if (CharNum != sizeof(ASCIIMsg))
       {
           xUARTTimerReset(TIMEOUT_UART_WRITE);
           while (!bUARTTimeout) {}
       }
    }
}
