#include "rw_uart.h"

/*------------------------------------ UART Communication --------------------------------------------*/

//Initialize all UART units
void xUARTInit(void)
{
    UCHAR i;
    UCHAR UARTRegAddress;
    bUARTTimeout = FALSE; //global variable

    //LCR = 0x08 (DLAB = 1)
    UARTRegAddress = UART_START + 3;

    for (i = 0; i < NUM_POWER_UNITS; i++)
    {
        ucUARTRW(0x08, UARTRegAddress, TRUE);
        UARTRegAddress += UART_REGNUM;
    }

    //LS = 0x18, MS = 0 (1.8432MHz Crystal, 4800 Baud, Decimal 24)
    UARTRegAddress = UART_START;

    for (i = 0; i < NUM_POWER_UNITS; i++)
    {
        ucUARTRW(0x18, UARTRegAddress, TRUE);
        ucUARTRW(0, UARTRegAddress + 1, TRUE);
        UARTRegAddress += UART_REGNUM;
    }

    //LCR = 0x07 (8 bits, 1 Stop, No Parity, DLAB = 0)
    UARTRegAddress = UART_START + 3;

    for (i = 0; i < NUM_POWER_UNITS; i++)
    {
        ucUARTRW(0x07, UARTRegAddress, TRUE);
        UARTRegAddress += UART_REGNUM;
    }
}

//Read/Write data to single UART register
UCHAR ucUARTRW(UCHAR ucRegister, USHORT usAddress, BOOL bWrite)
{
    if (bWrite)
    {
        //PD0-PD3, PC4-PC7 configured as outputs
        GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
        GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

        //Write to UART register
        xWriteAddress(usAddress);
        xWrite8BitTable(ucRegister);
        return 0;
    }
    else
    {
        //PD0-PD3, PC4-PC7 configured as inputs
        GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
        GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

        //Read UART register
        xWriteAddress(usAddress);
        return xRead8BitTable();
    }
}

//Reset UART
void xUARTTimerReset(ULONG ulTimerLoadValue)
{
    bUARTTimeout = FALSE;
    TimerLoadSet(TIMER2_BASE, TIMER_BOTH, ulTimerLoadValue);
    TimerEnable(TIMER2_BASE, TIMER_BOTH);
}

void Timer2IntHandler(void)
{
    uint32_t ui32status;
    ui32status = TimerIntStatus(TIMER2_BASE, TRUE);
    TimerIntClear(TIMER2_BASE, ui32status);
    bUARTTimeout = TRUE;
}
