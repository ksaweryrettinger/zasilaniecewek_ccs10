#pragma once

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include <rw_tables.h>
#include <string.h>
#include <stdlib.h>
#include "tm4c123gh6pm.h"
#include "params.h"
#include "mbreg.h"

/* ----------------------- Function prototypes ------------------------------------*/

void xUARTInit(void);                 //initialize INS8250 UART modules
UCHAR ucUARTRW(UCHAR, USHORT, BOOL);  //R/W operation on single INS8250 UART register
void xUARTTimerReset(ULONG);          //reset INS8250 UART timeout timer

/* ----------------------- Global variables ---------------------------------------*/

volatile BOOL bUARTTimeout;
