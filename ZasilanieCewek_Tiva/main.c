// Zasilanie Cewek Korekcyjnych - TM4C123GH6PM
// Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include <string.h>
#include "rw_tables.h"
#include "rw_uart.h"
#include "rw_psu.h"
#include "tm4c123gh6pm.h"
#include "params.h"
#include "mbreg.h"

int main(void)
{
    /*------------------------ Variables ------------------------------------------------*/

    //ASCII messages
    const CHAR ASCIISetRemote[] = {'S', 'R', 0x0D, 0x0A};
    const CHAR ASCIISetShortMessage[] = {'S', 'M', '0', 0x0D, 0x0A};
    const CHAR ASCIISetEchoOff[] = {'S', 'B', '0', 0x0D, 0x0A};
    uint8_t unitID = 1;

    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*------------------------ FPU Module -----------------------------------------------*/

    FPULazyStackingEnable();
    FPUEnable();

    /*------------------------ GPIO Pin Configuration -----------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    //GPIO Inputs
    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_4);

    //GPIO Outputs
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7);
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5);
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_5);
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_6);

    //Default GPIO Output states
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7, 0);
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    //PD0-PD3, PC4-PC7 configured as inputs (default Modbus Coil state)
    GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

    // UART (Modbus)
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // I2C
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    //SSI
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    GPIOPinConfigure(GPIO_PA5_SSI0TX);
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_5);

    /*------------------------------ I2C Configuration ----------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);
     I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), FALSE); //standard speed

     /*------------------------------ I2C Timeout Timer ----------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
     TimerConfigure(TIMER1_BASE, TIMER_CFG_ONE_SHOT);
     IntEnable(INT_TIMER1A);
     TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);

     /*------------------------------ UART Timeout Timer ----------------------------------*/

     SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
     TimerConfigure(TIMER2_BASE, TIMER_CFG_ONE_SHOT);
     IntEnable(INT_TIMER2A);
     TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

    /*------------------------------- SSI Configuration ---------------------------------*/

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 8000000, 8);
    SSIEnable(SSI0_BASE);

    /*------------------------------ MODBUS Initialization ------------------------------*/

    eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    eMBEnable();

    /*------------------------------ PSU Initialization ---------------------------------*/

    while (1)
    {
        //Modbus stack
        (void) eMBPoll();

        //UART Initialization
        if (usMBHoldingReg[HOLDING_RESET_UART - 1] == 1)
        {
            (void) xUARTInit();
            usMBHoldingReg[HOLDING_RESET_UART - 1] = 0; //clear holding register
        }

        //PSU Initialization
        if (usMBHoldingReg[HOLDING_RESET_PSU - 1] != 0)
        {
            if ((usMBHoldingReg[HOLDING_RESET_PSU - 1] >> (unitID - 1)) & 1)
            {
                /* TODO: Read PSU Status with "?0"
                 *
                 *      Response will either be "L operation", or "R" (short-messages enabled).
                 *      Check for echos when sending the ?0 message.
                 *
                 *      If in Local mode and echos are being received, the PSU has been reset.
                 *      Continue with the initialization of the device. Set current to zero.
                 *
                 *      If in Remote mode and echos are not being received, the PSU has not been reset,
                 *      and is working as before. Skip initialization, read the latest current and
                 *      treat that as the setting.
                 *
                 *      If in Local mode but echos are not being received, query the user whether they
                 *      want to reset the PSU. Is yes, then continue with initialization and reset current.
                 *      If no, then read skip initialization, read the latest current and treat that as the setting.
                 *
                 *      Store current settings in Modbus.
                 *
                 *  Linux:
                 *
                 *      When starting program, attempt to initalize all PSUs.
                 *      Read the latest current settings.
                 *      Prompt user in the event of PSU Local mode with no echos.
                 *
                 */

                if (0)
                {
                    xSetupSinglePSU(ASCIISetRemote, unitID); //set remote mode
                    xSetupSinglePSU(ASCIISetShortMessage, unitID); //set short messages
                    xSetupSinglePSU(ASCIISetEchoOff, unitID); //disable echos
                }

                //Confirm successful initialization and reset flags
                bUnitInitialized[unitID - 1] = TRUE;
                usMBHoldingReg[HOLDING_RESET_PSU - 1] &= ~(1 << (unitID - 1));
            }

            //Check one unit at a time to avoid delays
            if (unitID < NUM_POWER_UNITS) unitID++;
            else unitID = 1;
        }

        //Read current from initialized units
        (void) xReadCurrent();
    }
}

/*------------------------------------ MODBUS Callback Functions -----------------------------------------*/

/* Callback function - Read/Write Holding Register */
eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    USHORT usRegIndex;
    USHORT usBuffer;

    usAddress -= 1;

    if ((usAddress >= REG_HOLDING_START) && (usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS)) //Modbus Holding Registers
    {
        usRegIndex = (USHORT) (usAddress - REG_HOLDING_START);

        switch (eMode)
        {
            case MB_REG_READ:

                while (usNRegs > 0)
                {
                    *pucRegBuffer++ = (UCHAR)(usMBHoldingReg[usRegIndex] >> 8);
                    *pucRegBuffer++ = (UCHAR)(usMBHoldingReg[usRegIndex] & 0xFF);
                    usRegIndex++;
                    usNRegs--;
                }

                break;

            case MB_REG_WRITE:

                //Set new current value(s)
                if ((usAddress >= REG_HOLDING_START) && (usAddress + usNRegs <= REG_HOLDING_START + HOLDING_CURRENT_END))
                {
                    while (usNRegs > 0)
                    {
                        //Get next current value
                        usBuffer = *pucRegBuffer++ << 8;
                        usBuffer |= *pucRegBuffer++;

                        //Write new current value to PSU
                        xWriteCurrent(usBuffer, usRegIndex + 1);

                        //Update local buffer
                        usCurrentSettingsBCD[usRegIndex] = usBuffer;

                        //Update indices
                        usRegIndex++;
                        usNRegs--;
                    }
                }
                else if (usAddress + usNRegs == REG_HOLDING_START + HOLDING_RESET_UART)
                {
                    usMBHoldingReg[usRegIndex] = *pucRegBuffer++ << 8;
                    usMBHoldingReg[usRegIndex] |= *pucRegBuffer++;
                }
                else if (usAddress + usNRegs == REG_HOLDING_START + HOLDING_RESET_PSU)
                {
                    usMBHoldingReg[usRegIndex] = *pucRegBuffer++ << 8;
                    usMBHoldingReg[usRegIndex] |= *pucRegBuffer++;
                }
                else
                {
                    eStatus = MB_ENOREG;
                }

                break;
        }
    }
    else if ((usAddress + 1) >= (REG_8BIT_START + 1) && (usAddress + (usNRegs - 1)) <= REG_I2C)
    {
        switch (eMode)
        {
            case MB_REG_WRITE:

                //PD0-PD3, PC4-PC7 configured as outputs
                GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
                GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

                while (usNRegs > 0)
                {
                    usBuffer = *pucRegBuffer++ << 8;
                    usBuffer |= *pucRegBuffer++;

                    xWriteAddress(usAddress);

                    if (usAddress <= REG_8BIT_END) //8-bit table
                    {
                        xWrite8BitTable((UCHAR) (usBuffer & 0xFF));
                    }
                    else if (usAddress <= REG_16BIT_END) //16-bit table
                    {
                        xWrite16BitTable(usBuffer);
                    }
                    else if (usAddress == REG_I2C) //I2C
                    {
                        xWriteI2C((UCHAR) (usBuffer & 0xFF));
                    }

                    usAddress++;
                    usNRegs--;
                }
                break;

            case MB_REG_READ:

                //PD0-PD3, PC4-PC7 configured as inputs
                GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
                GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

                while (usNRegs > 0)
                {
                    xWriteAddress(usAddress);

                    if (usAddress <= REG_8BIT_END) //8-bit table
                    {
                        *pucRegBuffer++ = 0;
                        *pucRegBuffer++ = xRead8BitTable();
                    }
                    else if (usAddress <= REG_16BIT_END) //16-bit table
                    {
                        usBuffer = xRead16BitTable();
                        *pucRegBuffer++ = (UCHAR) (usBuffer >> 8);
                        *pucRegBuffer++ = (UCHAR) (usBuffer & 0xFF);
                    }
                    else if (usAddress == REG_I2C) //I2C
                    {
                        *pucRegBuffer++ = 0;
                        *pucRegBuffer++ = xReadI2C();
                    }

                    usAddress++;
                    usNRegs--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}

/* Callback function - Read/Write Coils */
eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode)
{
   return MB_ENOREG;
}

/* Callback function - Read Discrete Inputs */
eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete)
{
    return MB_ENOREG;
}

/* Callback function - Read Input Register */
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
    return MB_ENOREG;
}

void LEDStatus(BOOL bStatus)
{
    if (bStatus) GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_6, GPIO_PIN_6); //switch ON LED
    else GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_6, 0); //switch OFF LED
}

